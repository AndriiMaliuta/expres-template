const express = require("express");
const path = require("path");

const app = express();

app.get("/", (req, res) => {
  res.send("Hello from Express!");
});

const PORT = 80;

app.listen(PORT, () => console.log(`App listening on port ${PORT}!`));
